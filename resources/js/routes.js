import AllOutgoingCall from './components/AllOutgoingCall.vue';
import AddOutgoingCall from './components/AddOutgoingCall.vue';
import EditOutgoingCall from './components/EditOutgoingCall.vue';

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllOutgoingCall
    },
    {
        name: 'add',
        path: '/add',
        component: AddOutgoingCall
    },
    {
        name: 'edit',
        path: '/edit',
        component: EditOutgoingCall
    }
];