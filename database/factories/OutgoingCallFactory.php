<?php

namespace Database\Factories;

use App\Models\OutgoingCall;
use Illuminate\Database\Eloquent\Factories\Factory;

class OutgoingCallFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OutgoingCall::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){
        return [
            'name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'started_at' => $this->faker->dateTimeBetween($startDate = '-5 years', $endDate = '-4 years', $timezone = 'UTC'),
            'finished_at' => $this->faker->dateTimeBetween($startDate = '-3 years', $endDate = '-2 years', $timezone = 'UTC')
        ];
    }
}
