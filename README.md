![](presentation.gif)

# Fixu Demo CRUD

CRUD Básico de registros de llamadas telefónicas.

### Pre-Requisitos

Cosas que necesitas tener instalado para hacerlo funcionar:

-   Apache HTTP Server (2.4)
-   Laravel (8.x)
-   sqlite3 (3.27)

## Autor

-   **Oswaldo Graterol** - [oswaldog](https://gitlab.com/oswaldog)
