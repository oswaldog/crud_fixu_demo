<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('outgoingcalls', 'App\Http\Controllers\OutgoingCallController@index');
Route::get('outgoingcall/edit/{id}', 'App\Http\Controllers\OutgoingCallController@edit');
Route::post('outgoingcall', 'App\Http\Controllers\OutgoingCallController@add');
Route::put('outgoingcall', 'App\Http\Controllers\OutgoingCallController@update');
Route::delete('outgoingcall/{id}', 'App\Http\Controllers\OutgoingCallController@delete');
