<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OutgoingCall extends Model
{
    use HasFactory;

    protected $fillable = ['name','phone','started_at','finished_at'];

    protected $casts = [
        'started_at' => 'datetime:Y-m-d\TH:i',
        'finished_at' => 'datetime:Y-m-d\TH:i',
    ];

}
