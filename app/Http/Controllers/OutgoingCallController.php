<?php

namespace App\Http\Controllers;

use App\Models\OutgoingCall;
use Illuminate\Http\Request;
use App\Http\Requests\OutgoingCallRequest;

class OutgoingCallController extends Controller
{

    public function index(){

        return OutgoingCall::all();

    }


    public function add(OutgoingCallRequest $request)
    {
        $validated = $request->validated();

        OutgoingCall::create($request->all());

        return response()->json('Registro completado!!!');

    }


    public function update(OutgoingCallRequest $request)
    {

        $id = $request->input('id');
        
        $validated = $request->validated();

        $outgoingCall = OutgoingCall::find($id);
        
        $outgoingCall->update($request->all());

        return response()->json('Registro actualizado!!!');
    }


    public function delete($id)
    {
        $outgoingCall = OutgoingCall::find($id);

        $outgoingCall->delete();

        return response()->json('Registro eliminado!!!');

    }


    public function edit($id)
    {
        $outgoingCall = OutgoingCall::find($id);
        
        return response()->json($outgoingCall);
        
    }

}
